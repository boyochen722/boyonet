import os
import argparse
from torch.utils.tensorboard import SummaryWriter

from modules.data_loader import get_processed_dataloaders
from modules.model_trainer import train_model
from modules.genetic_algorithm import produce_new_generation
from modules.model_IO import prepare_model_save_path, construct_model
from modules.experiment_helper import parse_experiment_settings, \
    print_sub_exp_settings  # , record_model_settings_and_score


def execute_sub_exp(sub_exp_settings, repeat_all_subexp):
    experiment_name = sub_exp_settings['experiment_name']
    sub_exp_name = sub_exp_settings['sub_exp_name']
    log_path = f'../logs/{experiment_name}/{sub_exp_name}'

    print(f'Executing sub-experiment: {sub_exp_name}')
    if not repeat_all_subexp and os.path.isdir(log_path):
        print('Sub-experiment already done before, skipped ಠ_ಠ')
        return 0
    print_sub_exp_settings(sub_exp_settings)

    dataloaders = get_processed_dataloaders(**sub_exp_settings['data'])
    model_save_path = prepare_model_save_path(experiment_name, sub_exp_name)

    if 'produce_new_generation' in sub_exp_settings:
        summary_writer = SummaryWriter(log_path)
        produce_new_generation(
            summary_writer, **sub_exp_settings['produce_new_generation']
        )
        summary_writer.close()
    else:
        model = construct_model(sub_exp_settings['model'])
        summary_writer = SummaryWriter(log_path)
        best_valid_score, best_GA_score = train_model(
            model, dataloaders, summary_writer, model_save_path,
            **sub_exp_settings['training_settings']
        )
        # record_model_settings_and_score(
        #     summary_writer, sub_exp_settings['model'],
        #     best_valid_score, best_GA_score
        # )
        summary_writer.close()
    return 1


def main(experiment_path, GPU_limit, repeat_all_subexp):
    while 1:
        # parse yaml to get experiment settings
        experiment_list = parse_experiment_settings(experiment_path)

        executed_sub_exp = 0
        for sub_exp_settings in experiment_list:
            executed_sub_exp += execute_sub_exp(sub_exp_settings, repeat_all_subexp)

        if not executed_sub_exp or repeat_all_subexp:
            print('all experiments are done, nothing more to do!')
            break


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'experiment_path',
        help='name of the experiment setting, should match one of them file name in experiments folder'
    )
    parser.add_argument('--GPU_limit', type=int, default=-1)
    parser.add_argument('-r', '--repeat_all_subexp', action='store_true')
    parser.add_argument('-d', '--CUDA_VISIBLE_DEVICES', type=str, default='')
    args = parser.parse_args()
    if args.CUDA_VISIBLE_DEVICES:
        os.environ['CUDA_VISIBLE_DEVICES'] = args.CUDA_VISIBLE_DEVICES
    main(args.experiment_path, args.GPU_limit, args.repeat_all_subexp)
