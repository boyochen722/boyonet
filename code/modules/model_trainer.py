import torch
import arrow
from modules.training_helper import inspect_model
from modules.model_IO import save_model
from tqdm import tqdm


def timeit(method):
    def timed_function(*args, **kw):
        t1 = arrow.now()
        result = method(*args, **kw)
        t2 = arrow.now()
        print(f'{method.__name__}, runtime: {t2-t1}')
        return result
    return timed_function


def remember(model, train_dataloaders, STDP_freq):
    for images, labels in tqdm(train_dataloaders, desc='rem'):
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        images, labels = images.to(device), labels.to(device)
        model.remember(images, labels, STDP_freq)


@timeit
def evaluate(model, dataloaders, summary_writer, epoch_index):
    print(f'Now evaluating: epoch #{epoch_index}')
    accuracy_dict, ablation_weighted_score = inspect_model(model, dataloaders, summary_writer, epoch_index)

    return accuracy_dict['valid'], ablation_weighted_score


@timeit
def sleep(model):
    # checkpoint_path = f'../saved_models/checkpoint_before_PCA_{np.random.randint(1000000000)}'
    # save_model(model, checkpoint_path)
    model.sleep()
    # os.remove(checkpoint_path+'.pickle')


def train_model(
    model, dataloaders, summary_writer, model_save_path,
    max_epoch, min_epoch, sleep_freq, evaluate_freq, STDP_freq
):
    best_valid_accuracy = 0
    best_GA_score = 0
    for epoch_index in range(1, max_epoch+1):
        remember(model, dataloaders['train'], STDP_freq)

        if epoch_index % evaluate_freq == 0:
            valid_score, GA_score = evaluate(model, dataloaders, summary_writer, epoch_index)
            if epoch_index >= min_epoch and best_valid_accuracy < valid_score:
                best_valid_accuracy = valid_score
                best_GA_score = GA_score
                save_model(model, model_save_path)

        if epoch_index % sleep_freq == 0 and epoch_index != max_epoch:
            sleep(model)
            # remember with STDP_freq=0, only for adjust thresholds before evaluate
            remember(model, dataloaders['train'], STDP_freq=0)
            valid_score, GA_score = evaluate(model, dataloaders, summary_writer, epoch_index+1)
            # given that after sleep the parameter is not at a stable stage,
            # never use the model here.
            # if epoch_index >= min_epoch and best_valid_accuracy < valid_score:
            #     best_valid_accuracy = valid_score
            #     save_model(model, model_save_path)

    return float(best_valid_accuracy), float(best_GA_score)
