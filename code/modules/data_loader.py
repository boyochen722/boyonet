import torchvision
from torch.utils.data import DataLoader, random_split


def get_processed_dataloaders(dataset_name, batch_size):
    dataset_class = getattr(torchvision.datasets, dataset_name)
    train_dataset = dataset_class(
        root='../data',
        train=True,
        transform=torchvision.transforms.ToTensor(),  # Image data normalized to range [0, 1]
        download=True
    )
    test_dataset = dataset_class(
        root='../data',
        train=False,
        transform=torchvision.transforms.ToTensor(),  # Image data normalized to range [0, 1]
        download=True
    )

    valid_train_ratio = 0.1
    train_size = int(len(train_dataset) * (1-valid_train_ratio))
    valid_size = int(len(train_dataset) * valid_train_ratio)
    train_dataset, valid_dataset = random_split(train_dataset, [train_size, valid_size])

    datasets = {
        'train': train_dataset,
        'valid': valid_dataset,
        'test': test_dataset
    }

    dataloaders = {
        phase: DataLoader(
            dataset=dataset,
            batch_size=batch_size,
            shuffle=True,
            drop_last=False,
            num_workers=40,
            pin_memory=True
        ) for phase, dataset in datasets.items()
    }

    return dataloaders
