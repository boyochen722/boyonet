import pprint
import torch
from sklearn.metrics import confusion_matrix as CM
from modules.model_inspector import get_cortex_dict, draw_spike_trains, \
    draw_heat_map, get_nested_shape, draw_bar_chart
from modules.utils import mean_excluding_zeros

cached_sample_data = None


def get_sample_data(dataloaders=None, use_cache=True, max_num=3):
    global cached_sample_data

    if use_cache is True and cached_sample_data:
        return cached_sample_data

    sample_data_dict = {}
    for phase in dataloaders:
        for images, labels in dataloaders[phase]:
            device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
            images, labels = images.to(device), labels.to(device)
            sample_data_dict[phase] = (images[:max_num], labels[:max_num])
            break

    if use_cache is True:
        cached_sample_data = sample_data_dict
    return sample_data_dict


def evaluate_accuracy(model, dataloaders, valid_only=False, with_CM=True):
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    result = {}
    if valid_only:
        phases = ['valid']
    else:
        phases = ['train', 'test', 'valid']

    valid_labels = []
    valid_preds = []

    for phase in phases:
        correct_pred_count = 0
        all_pred_count = 0
        for images, labels in dataloaders[phase]:
            images, labels = images.to(device), labels.to(device)
            batch_size, height, width, channel = images.shape
            model.cortex.init_state(batch_size, device)
            pred_probabilities = model(images)
            pred_classes = torch.argmax(pred_probabilities, dim=1)
            correct_pred_count += (pred_classes == labels).sum().item()
            all_pred_count += len(labels)
            if phase == 'valid':
                valid_labels.append(labels)
                valid_preds.append(pred_classes)
        result[phase] = correct_pred_count/all_pred_count

    if with_CM:
        confusion_matrix = CM(
            torch.cat(valid_labels).cpu().numpy(),
            torch.cat(valid_preds).cpu().numpy()
        )
        return result, confusion_matrix

    return result


def ablation_test(model, dataloaders, cortex_dict, valid_score):
    ablation_score_dict = {}
    for cortex_id, cortex in cortex_dict.items():
        backup_cortex_kernel = cortex.kernel.clone()
        in_len, out_len = cortex.shape
        cortex.kernel[:in_len, :out_len] = 0
        ablation_score_dict[cortex_id] = valid_score - evaluate_accuracy(
            model, dataloaders, valid_only=True, with_CM=False
        )['valid']
        cortex.kernel = backup_cortex_kernel
    return ablation_score_dict


def draw_ablation_chart(ablation_score_dict, valid_score, cortex_dict, show):
    input_nv_len = [
        len(cortex_dict[cortex_name].input_nv)
        for cortex_name in ablation_score_dict
    ]
    ablation_chart = draw_bar_chart(
        ablation_score_dict, hlines=[valid_score], bottom=0.1,
        labels=input_nv_len, show=show,
        title='Ablation test'
    )
    return ablation_chart


def inspect_model(model, dataloaders, summary_writer, epoch_index, level=3, show_fig=False):
    accuracy_dict, confusion_matrix = evaluate_accuracy(model, dataloaders)

    if level <= 1:
        return accuracy_dict

    summary_writer.add_image(
        '[valid] confusion_matrix',
        draw_heat_map(torch.tensor(confusion_matrix))[None, :], global_step=epoch_index, dataformats='NHWC'
    )

    cortex_dict = get_cortex_dict(model.cortex)

    ablation_score_dict = ablation_test(model, dataloaders, cortex_dict, accuracy_dict['valid'])
    ablation_chart = draw_ablation_chart(
        ablation_score_dict, accuracy_dict['valid'], cortex_dict, show=show_fig
    )

    if level <= 2:
        return accuracy_dict

    nested_shape = get_nested_shape(model.cortex)
    model_shape_info = pprint.pformat(nested_shape, indent=4)
    print('---------------- model shape ----------------')
    print(model_shape_info)
    print('---------------------------------------------')
    
    cortex_amplifier = {
        cortex_name: cortex.amplifier
        for cortex_name, cortex in cortex_dict.items()
    }

    cortex_input_thresholds = {
        cortex_name: cortex.input_nv.thresholds
        for cortex_name, cortex in cortex_dict.items()
    }

    cortex_output_thresholds = {
        cortex_name: cortex.output_nv.thresholds
        for cortex_name, cortex in cortex_dict.items()
    }

    cortex_kernel = {
        cortex_name: cortex.kernel
        for cortex_name, cortex in cortex_dict.items()
    }

    # cortex_kernel_heat_map = {
    #     cortex_name: draw_heat_map(cortex.kernel, show=show_fig)
    #     for cortex_name, cortex in cortex_dict.items()
    # }

    ablation_weighted_score = sum(
        [score * 2**((len(name)-1)/2) for name, score in ablation_score_dict.items()]
    ) - ablation_score_dict['A']

    sample_data_dict = get_sample_data(dataloaders)
    spike_train_dict = draw_spike_trains(sample_data_dict, model)

    # ------ write to tensorboard ------
    for cortex_name, amplifier in cortex_amplifier.items():
        summary_writer.add_scalar(
            f'[all] cortex amplifier: {cortex_name}',
            amplifier, global_step=epoch_index
        )
    for cortex_name, kernel in cortex_kernel.items():
        summary_writer.add_histogram(
            f'[all] cortex kernel: {cortex_name}',
            kernel, global_step=epoch_index
        )
    for cortex_name, threshold in cortex_input_thresholds.items():
        summary_writer.add_histogram(
            f'[all] input threshold: {cortex_name}',
            threshold, global_step=epoch_index
        )
        # summary_writer.add_scalar(
        #     f'[all] mean input threshold: {cortex_name}',
        #     mean_excluding_zeros(threshold).cpu().numpy(),
        #     global_step=epoch_index
        # )
    for cortex_name, threshold in cortex_output_thresholds.items():
        summary_writer.add_histogram(
            f'[all] output threshold: {cortex_name}',
            threshold, global_step=epoch_index
        )
        # summary_writer.add_scalar(
        #     f'[all] mean output threshold: {cortex_name}',
        #     mean_excluding_zeros(threshold).cpu().numpy(),
        #     global_step=epoch_index
        # )

    # for cortex_name, figure in cortex_kernel_heat_map.items():
    #     summary_writer.add_image(
    #         f'[all] cortex kernel heat map: {cortex_name}',
    #         figure[None, :], global_step=epoch_index, dataformats='NHWC'
    #     )

    summary_writer.add_text('model_shape_info', model_shape_info, global_step=epoch_index)

    for phase, value in accuracy_dict.items():
        summary_writer.add_scalar(f'[{phase}] accuracy', value, global_step=epoch_index)

    # use ablation_weighted_score as GA_score
    summary_writer.add_scalar('GA_score', ablation_weighted_score, global_step=epoch_index)

    for figure_name, figure in spike_train_dict.items():
        summary_writer.add_image(
            figure_name, figure,
            global_step=epoch_index, dataformats='NHWC'
        )

    summary_writer.add_image(
        'Ablation chart',
        ablation_chart[None, :], global_step=epoch_index, dataformats='NHWC'
    )

    return accuracy_dict, ablation_weighted_score
