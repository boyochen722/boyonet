import numpy as np
import torch
from collections import defaultdict
import matplotlib.pyplot as plt
from PIL import Image
from modules.utils import remove_nan
from io import BytesIO


# avoid PIL.Image.DecompressionBombError when hidden neurons increased.
Image.MAX_IMAGE_PIXELS = 1000000000


def get_cortex_dict(cortex_obj):
    cortex_dict = {
        cortex_obj.cortex_id: cortex_obj
    }
    for i, subcortex in enumerate(cortex_obj.subcortexs):
        cortex_dict.update(
            get_cortex_dict(subcortex)
        )

    return cortex_dict


def _get_cortex_spikes(cortex):
    receiver_spikes = [cortex.output_nv.current_spikes] + [
        subcortex.input_nv.current_spikes
        for subcortex in cortex.subcortexs
    ]

    return torch.cat(receiver_spikes, dim=1)


def _get_spike_train_dict(model, images, cortex_dict):
    batch_size, height, width, channel_num = images.shape
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model.cortex.init_state(batch_size, device)

    spike_trains = defaultdict(list)
    for spikes in model.image_encoder(images):
        model.cortex(spikes, adjust_thresholds=False)

        for cortex_name, cortex in cortex_dict.items():
            spike_vector = _get_cortex_spikes(cortex)
            spike_trains[cortex_name].append(spike_vector)

    h_lines_dict = defaultdict(list)
    for cortex_name, cortex in cortex_dict.items():
        spike_trains[cortex_name] = torch.stack(spike_trains[cortex_name], axis=-1)
        i = len(cortex.output_nv)
        for subcortex in cortex.subcortexs:
            h_lines_dict[cortex_name].append(i)
            i += len(subcortex.input_nv)

    return spike_trains, h_lines_dict


def _plt_to_np_array(plt, dpi=100):
    # matplotlib default dpi = 100, set it lower to speed up when the image is big.

    buf = BytesIO()
    plt.savefig(buf, format='jpeg', dpi=dpi)
    buf.seek(0)

    image = Image.open(buf)
    image_np_array = np.array(image)

    buf.close()

    return image_np_array


def draw_spike_trains(sample_data_dict, model):
    cortex_dict = get_cortex_dict(model.cortex)
    image_dict = defaultdict(list)
    for phase, (images, labels) in sample_data_dict.items():
        spike_train_dict, h_lines_dict = _get_spike_train_dict(
            model, images, cortex_dict
        )
        for cortex_name, spike_train_batch in spike_train_dict.items():
            figure_name = f'[{phase}] spike train: {cortex_name}'
            for spike_train, label in zip(
                spike_train_batch, labels
            ):
                image_dict[figure_name].append(
                    draw_heat_map(
                        spike_train,
                        length=10,
                        title=f'{cortex_name}, label: {label}',
                        h_lines=h_lines_dict[cortex_name]
                    )
                )

    for figure_name in image_dict:
        image_dict[figure_name] = np.stack(image_dict[figure_name])
    return image_dict


def draw_heat_map(matrix, length=6, title=None, show=False, h_lines=[]):
    shape_ratio = matrix.shape[0] / matrix.shape[1]
    figsize = np.array([1, max(shape_ratio, 0.2)]) * length

    space_for_notation = length/10
    figsize[0] += space_for_notation

    plt.figure(figsize=figsize)
    if title is not None:
        plt.title(title)
    matrix = remove_nan(matrix, 0).cpu().numpy()
    plt.pcolormesh(matrix, cmap="YlOrRd")
    plt.colorbar()
    
    ax = plt.gca()
    for line in h_lines:
        ax.hlines(line, xmin=0, xmax=matrix.shape[1], colors='k', linewidth=1)

    ax.invert_yaxis()

    if show:
        plt.show()
    RGB_matrix = _plt_to_np_array(plt)
    plt.close()
    return RGB_matrix


def draw_bar_chart(D, hlines=[], bottom=None, title=None, labels=None, show=False):
    keys, values = zip(*D.items())
    bars = plt.bar(keys, values, bottom=bottom)
    plt.xticks(rotation=45)
    plt.ylim(0.1, 1)
    for hline in hlines:
        plt.axhline(y=hline, c="r", ls="--", lw=2)
    if labels is not None:
        plt.bar_label(bars, labels=labels, label_type='center', fontsize=15)
    if title is not None:
        plt.title(title)
    if show:
        plt.show()
    RGB_matrix = _plt_to_np_array(plt)
    plt.close()
    return RGB_matrix


def get_nested_shape(cortex):
    input_len, output_len = cortex.shape
    kernel_shape = cortex.kernel.shape
    subcortex_shapes = [
        get_nested_shape(subcortex)
        for subcortex in cortex.subcortexs
    ]
    return {
        'cortex_id': cortex.cortex_id,
        'input_len': input_len,
        'output_len': output_len,
        'kernel_shape': kernel_shape,
        'amplifier': cortex.amplifier,
        'p_rate': cortex.potentiation_rate,
        'subcortexs': subcortex_shapes
    }
