import torch


class PoissonEncoder():
    def __init__(self, simulation_time, max_rate):
        self.simulation_time = simulation_time
        self.max_rate = max_rate

    def __call__(self, images, ):
        batch_size, channels, height, width = images.shape
        pixels = height * width * channels
        flatten_images = images.view([batch_size, pixels]) * self.max_rate
        for t in range(self.simulation_time):
            random_score = torch.rand(batch_size, pixels, device=flatten_images.device)
            spikes = (flatten_images > random_score).float()
            yield spikes


class ProbeEncoder():
    def __init__(self, kernel_size, stride):
        self.kernel_size = kernel_size
        self.stride = stride
        self.movement = {
            0: [0, 1],
            1: [1, 0],
            2: [0, -1],
            3: [-1, 0]
        }

    def _get_following_steps(self, H, W, pre_y, pre_x, direction):
        now_y = pre_y + self.movement[direction][0]
        now_x = pre_x + self.movement[direction][1]
        if not (0 <= now_y < H and 0 <= now_x < W):
            return []
        if self.visited_map[now_y][now_x]:
            return []
        self.visited_map[now_y][now_x] = 1

        return [(now_y, now_x)] + \
            self._get_following_steps(H, W, now_y, now_x, direction) + \
            self._get_following_steps(H, W, now_y, now_x, (direction+1) % 4)

    def _generate_positions(self, H, W):
        self.visited_map = [[False for i in range(W)] for j in range(H)]
        return reversed(self._get_following_steps(H, W, 0, -1, direction=0))

    def __call__(self, images):
        batch_size, channels, height, width = images.shape
        H = int((height - self.kernel_size) / self.stride) + 1
        W = int((width - self.kernel_size) / self.stride) + 1

        for y, x in self._generate_positions(H, W):
            h_start = y * self.stride
            h_end = h_start + self.kernel_size
            w_start = x * self.stride
            w_end = w_start + self.kernel_size
            probe = images[:, :, h_start:h_end, w_start:w_end]
            pixels = self.kernel_size * self.kernel_size * channels
            flatten_images = probe.view([batch_size, pixels])
            random_score = torch.rand(batch_size, pixels, device=flatten_images.device)
            spikes = (flatten_images > random_score).float()
            yield spikes


def get_image_encoder(encoder, **args):
    if encoder == 'poisson':
        return PoissonEncoder(**args)
    if encoder == 'probe':
        return ProbeEncoder(**args)
