import os
import pathlib
import yaml
import pickle
import torch
from modules.yielding_ontology.boyonet import BoyoNet
from modules.utils import move_to_device


def prepare_model_save_path(experiment_name, sub_exp_name):
    if not os.path.isdir('../saved_models'):
        os.mkdir('../saved_models')

    saving_folder = '../saved_models/' + experiment_name
    if not os.path.isdir(saving_folder):
        os.mkdir(saving_folder)

    model_save_path = saving_folder + '/' + sub_exp_name
    return model_save_path


def load_model_settings(gene_id):
    gene_path = f'gene_library/{gene_id}.yaml'
    if not os.path.isfile(gene_path):
        return None
    with open(gene_path, 'r') as file:
        gene_model_settings = yaml.full_load(file)
    return gene_model_settings


def save_model_settings(model_settings, gene_id):
    gene_path = f'gene_library/{gene_id}.yaml'

    # mkdir if not exist
    pathlib.Path(gene_path).parent.mkdir(parents=True, exist_ok=True)

    with open(gene_path, 'w+') as f:
        yaml.dump(model_settings, f)
    return


def model_setting_update(model_settings, updates):
    for key, value in updates.items():
        if key == 'gene_id':
            continue
        if type(value) is dict and key in model_settings:
            model_setting_update(model_settings[key], value)
        else:
            model_settings[key] = value


def get_model_gene(model_settings):
    if 'gene_id' in model_settings:
        gene_model_settings = load_model_settings(model_settings['gene_id'])
        model_setting_update(gene_model_settings, model_settings)
        return gene_model_settings
    return model_settings


def construct_model(model_settings):
    if 'load_from' in model_settings:
        load_from_experiment = model_settings['load_from']['experiment_name']
        load_from_sub_exp = model_settings['load_from']['sub_exp_name']
        model = load_model(load_from_experiment, load_from_sub_exp)
    else:
        model = BoyoNet(**get_model_gene(model_settings))

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = move_to_device(model, device)
    return model


def save_model(model, model_save_path):
    if model_save_path is None:
        return
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model.cortex.init_state(batch_size=0, device=device)

    # mkdir if not exist
    pathlib.Path(model_save_path).parent.mkdir(parents=True, exist_ok=True)

    with open(f'{model_save_path}.pickle', 'wb') as fp:
        pickle.dump(model, fp, protocol=pickle.HIGHEST_PROTOCOL)


def load_model(load_from_experiment, load_from_sub_exp):
    load_from_path = prepare_model_save_path(load_from_experiment, load_from_sub_exp)
    with open(f'{load_from_path}.pickle', 'rb') as fp:
        model = pickle.load(fp)
    return model
