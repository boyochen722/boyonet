import copy
import numpy as np
from modules.model_IO import load_model_settings, save_model_settings, model_setting_update
from modules.utils import get_log_df


def decompress_GA_settings(
    base_exp_settings, generation, population, selection, mutation_rate, init_gene_ids=[]
):

    def generation_production_sub_exp(gen_i):
        sub_exp_settings = copy.deepcopy(base_exp_settings)
        sub_exp_settings['produce_new_generation'] = {
            'experiment_name': sub_exp_settings['experiment_name'],
            'sub_exp_name': sub_exp_settings['sub_exp_name'],
            'generation_i': gen_i,
            'selection': selection,
            'population': population,
            'mutation_rate': mutation_rate,
            'min_epoch': sub_exp_settings['training_settings']['min_epoch'],
            'overwrite': sub_exp_settings.get('model', {})
        }
        sub_exp_settings['sub_exp_name'] += f'/{gen_i}/production'
        return sub_exp_settings

    experiment_name = base_exp_settings['experiment_name']
    sub_exp_list = []
    for i in range(1, generation+1):
        sub_exp_list.append(generation_production_sub_exp(i))
        for j in range(1, population+1):
            base_sub_exp_name = base_exp_settings['sub_exp_name']
            sub_exp_settings = copy.deepcopy(base_exp_settings)
            sub_exp_settings['sub_exp_name'] = f'{base_sub_exp_name}/{i}/{i}-{j}'
            sub_exp_settings['model'] = {
                'gene_id': f'{experiment_name}/{base_sub_exp_name}/{i}-{j}'
            }
            sub_exp_list.append(sub_exp_settings)

    # load all the init genes and record them in the gene library.
    if init_gene_ids:
        generation_0_genes = [load_model_settings(gene_id) for gene_id in init_gene_ids]
    else:
        generation_0_genes = [base_exp_settings['model']]

    sub_exp_name = base_exp_settings['sub_exp_name']
    for i, gene in enumerate(generation_0_genes):
        gene_id = f'{experiment_name}/{sub_exp_name}/0-{i+1}'
        save_model_settings(gene, gene_id)

    return sub_exp_list


def merge_gene(gene1, gene2, mutation_rate):
    new_gene = {}
    for key, value1 in gene1.items():
        value2 = gene2[key]
        if type(value1) is dict:
            new_gene[key] = merge_gene(value1, value2, mutation_rate)
        elif type(value1) is float:
            mean = (value1 + value2) / 2
            std = max(abs(mean)*mutation_rate, abs(value1 - value2) / 2)
            new_gene[key] = np.random.normal(mean, std)
        else:
            new_gene[key] = value1
    return new_gene


def reproduction(parent_gene_library, population, mutation_rate):
    gene_library = []
    while len(gene_library) < population:
        gene1, gene2 = np.random.choice(parent_gene_library, 2)
        gene_library.append(merge_gene(gene1, gene2, mutation_rate))
    return gene_library


def select_gene(experiment_name, sub_exp_name, selection, min_epoch, generation_i):
    if generation_i < 1:
        selected_gene = []
        j = 1
        selected_ids = set()
        while True:
            gene_id = f'{experiment_name}/{sub_exp_name}/0-{j}'
            loaded_gene = load_model_settings(gene_id)
            if not loaded_gene:
                break
            selected_ids.add(f'0-{j}')
            selected_gene.append(loaded_gene)
            j += 1
    else:
        log_path = f'../logs/{experiment_name}/{sub_exp_name}/{generation_i}'
        selected_ids = set()
        for by_column in ['GA_score', '[valid] accuracy']:
            log_df = get_log_df(log_path, by_column)
            # generation_log = log_df[log_df.experiment_id.str.startswith(f'{generation_i}-')]
            generation_log = log_df
            # exclude records right after the split
            filtered_log = generation_log[generation_log.step % 10 != 1]
            # only consider records great or eq to min_epoch
            filtered_log = filtered_log[generation_log.step >= min_epoch]

            # gene_rank = filtered_log.sort_values(by_column, ascending=False).drop_duplicates(['experiment_id']).experiment_id
            # use mean instead of max
            gene_rank = filtered_log.groupby('experiment_id')[by_column].mean().sort_values(ascending=False).index
            selected_ids.update(list(gene_rank)[:selection])

        full_ids = [f'{experiment_name}/{sub_exp_name}/{selected_id}' for selected_id in selected_ids]
        selected_gene = [load_model_settings(full_id) for full_id in full_ids]

    return selected_ids, selected_gene


def produce_new_generation(
    summary_writer, experiment_name, sub_exp_name, generation_i, selection, min_epoch, population, mutation_rate, overwrite
):
    mother_short_ids, mother_gene_library = select_gene(experiment_name, sub_exp_name, selection, min_epoch, generation_i-2)
    father_short_ids, father_gene_library = select_gene(experiment_name, sub_exp_name, selection, min_epoch, generation_i-3)
    parent_gene_library = mother_gene_library + father_gene_library
    new_generation = reproduction(parent_gene_library, population, mutation_rate)

    summary_writer.add_text('parent_gene_library', str(mother_short_ids | father_short_ids), global_step=generation_i)
    for i, gene in enumerate(new_generation):
        gene_id = f'{experiment_name}/{sub_exp_name}/{generation_i}-{i+1}'
        model_setting_update(gene, overwrite)
        save_model_settings(gene, gene_id)
