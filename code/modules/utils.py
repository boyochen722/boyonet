# import os
import copy
import torch
# import pandas as pd
# from tensorflow.python.summary.summary_iterator import summary_iterator


def remove_nan(tensor_with_nan, new_value):
    nan_mask = torch.isnan(tensor_with_nan)
    tensor_without_nan = torch.where(nan_mask, torch.tensor(new_value, dtype=tensor_with_nan.dtype), tensor_with_nan)
    return tensor_without_nan


def mean_excluding_zeros(tensor_with_zeros):
    non_zero_mask = tensor_with_zeros != 0
    non_zero_count = non_zero_mask.sum().float()
    non_zero_sum = tensor_with_zeros[non_zero_mask].sum()
    non_zero_mean = non_zero_sum / non_zero_count
    
    return non_zero_mean


def _recursive_update_cortex_settings(cortex, **kargs):
    for key, value in kargs.items():
        setattr(cortex, key, value)
    for subcortex in cortex.subcortexs:
        _recursive_update_cortex_settings(subcortex, **kargs)


def copied_model_with_parameters(model, **kargs):
    copied_model = copy.deepcopy(model)
    _recursive_update_cortex_settings(copied_model.cortex, **kargs)
    return copied_model


def move_to_device(obj, device):
    if isinstance(obj, torch.Tensor):
        return obj.to(device)
    elif isinstance(obj, list):
        return [move_to_device(item, device) for item in obj]
    elif isinstance(obj, dict):
        return {key: move_to_device(value, device) for key, value in obj.items()}
    elif hasattr(obj, '__dict__'):
        for key, value in obj.__dict__.items():
            setattr(obj, key, move_to_device(value, device))
        return obj
    else:
        return obj


def get_log_df(log_dir, by_column):
    pass
    # # get log_file_paths
    # log_file_paths = []
    # for (path, dirs, filenames) in os.walk(log_dir):
    #     for filename in filenames:
    #         if "events.out.tfevents" in filename:
    #             file_path = os.path.join(path, filename)
    #             log_file_paths.append(file_path)
    
    # # use this tool function to parse binary tensor into plain float.
    # def get_event_float_value(event):
    #     # To be fixed!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    #     # tensor_1x1 = tf.io.decode_raw(
    #     #     event.summary.value[0].tensor.tensor_content, tf.float32
    #     # )
    #     tensor_1x1 = 'QAQ'
    #     return float(tensor_1x1)
    
    # # parse and store as list of tuples
    # rows = []
    # for path in log_file_paths:
    #     exp_name = path.split('/')[-2]
    #     rows += [
    #         (exp_name, event.step, get_event_float_value(event))
    #         for event in summary_iterator(path) if event.summary.value and event.summary.value[0].tag == by_column
    #     ]
    
    # # transform into pandas dataframe
    # return pd.DataFrame(rows, columns=['experiment_id', 'step', by_column])
