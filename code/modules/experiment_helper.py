import yaml
import collections
import copy
from pathlib import Path
from modules.genetic_algorithm import decompress_GA_settings
# from tensorboard.plugins.hparams import api as hp
# from modules.model_IO import get_model_gene


def parse_experiment_settings(experiment_path, only_this_sub_exp=''):
    experiment_path = Path(experiment_path)
    if experiment_path.is_dir():
        exps_from_all_subpath = []
        subpaths = [subpath for subpath in experiment_path.iterdir()]
        for subpath in subpaths:
            exps_from_all_subpath += parse_experiment_settings(subpath, only_this_sub_exp)
        return exps_from_all_subpath

    # try:
    with open(experiment_path, 'r') as file:
        experiment_settings = yaml.full_load(file)

    shared_settings = experiment_settings['shared_settings']
    shared_settings['experiment_name'] = experiment_settings['experiment_name']

    def deep_update(source, overrides):
        for key, value in overrides.items():
            if isinstance(value, collections.abc.Mapping) and value:
                returned = deep_update(source.get(key, {}), value)
                source[key] = returned
            else:
                source[key] = value
        return source

    exp_list = []
    for sub_exp_overrides in experiment_settings.get('sub_experiments', []):
        repeat = sub_exp_overrides.pop('repeat', 1)
        genetic_algorithm = sub_exp_overrides.pop('genetic_algorithm', None)
        for i in range(1, repeat+1):
            sub_exp_settings = copy.deepcopy(shared_settings)
            sub_exp_settings = deep_update(sub_exp_settings, sub_exp_overrides)
            if repeat > 1:
                sub_exp_settings['sub_exp_name'] += f'_#{i}'
            if genetic_algorithm:
                GA_exp_settings_list = decompress_GA_settings(sub_exp_settings, **genetic_algorithm)
                exp_list += GA_exp_settings_list
            else:
                exp_list.append(sub_exp_settings)

    if only_this_sub_exp:
        for sub_exp in exp_list:
            if sub_exp['sub_exp_name'] == only_this_sub_exp:
                return sub_exp
        print('sub_exp not found!')
        return []

    return exp_list

    # except (yaml.YAMLError, Exception):
    #     return []


def print_sub_exp_settings(sub_exp_settings, indent_level=0):
    for key, value in sub_exp_settings.items():
        indent = '\t' * indent_level
        if type(value) is dict:
            print(f'{indent}{key}:')
            print_sub_exp_settings(sub_exp_settings[key], indent_level+1)
        else:
            print(f'{indent}{key}: {value}')


def _get_flatten_gene(gene, prefix=[], max_level=2, excluded_key=['init_cortex']):
    flatten_settings = {}
    for key, value in gene.items():
        if key in excluded_key:
            continue
        if type(value) is dict:
            flatten_settings.update(
                _get_flatten_gene(
                    value,
                    prefix=prefix+[key],
                    max_level=max_level,
                    excluded_key=excluded_key
                )
            )
        else:
            short_name = '/'.join(prefix[-max_level+1:]+[key])
            flatten_settings[short_name] = value
    return flatten_settings


# def record_model_settings_and_score(
#     summary_writer, model_settings, best_valid_score, best_GA_score
# ):
#     gene = get_model_gene(model_settings)
#     flatten_gene = _get_flatten_gene(gene)
#     hp.hparams(flatten_gene)
#     summary_writer.add_scalar('[HPARAMS] valid_score', best_valid_score, global_step=0)
#     summary_writer.add_scalar('[HPARAMS] GA_score', best_GA_score, global_step=0)
