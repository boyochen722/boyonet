import torch
from functools import partial
from modules.image_encoders import get_image_encoder
from modules.yielding_ontology.pca_cortex import PcaCortex as Cortex


class BoyoNet():
    def __init__(
        self, init_cortex, cortex_spec, image_encoder_spec, remember_settings
    ):
        self.image_encoder = get_image_encoder(**image_encoder_spec)
        self.cortex_constructor = partial(
            Cortex, **cortex_spec
        )

        self.cortex = self.construct_init_model('A', **init_cortex)
        self.cortex.fix_shape()
        self.cortex.input_latency = 0
        # self.cortex.fix_amplifier()

        def setup(
            self_cleaning_after_STDP, label_off_value, STDP_mechanism
        ):
            self.self_cleaning_after_STDP = self_cleaning_after_STDP
            self.label_off_value = label_off_value
            self.STDP_mechanism = STDP_mechanism

        setup(**remember_settings)

    def construct_init_model(self, name, shape, subcortexs=[]):
        constructed_subcortexs = []
        for i, subcortex in enumerate(subcortexs, 1):
            sub_name = f'{name}-{i}'
            constructed_subcortexs.append(
                self.construct_init_model(sub_name, **subcortex)
            )
        new_cortex = self.cortex_constructor(
            cortex_id=name,
            input_len=shape[0],
            output_len=shape[1],
            subcortexs=constructed_subcortexs
        )
        return new_cortex

    def sleep(self):
        self.cortex.self_cleaning()
        self.cortex.splitting(self.cortex_constructor)
        return

    def remember(self, images, labels, STDP_freq):
        batch_size, channel, height, width = images.shape
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.cortex.init_state(batch_size, device)

        STDP_flag = False

        for i, spikes in enumerate(self.image_encoder(images)):
            _ = self.cortex(spikes, adjust_thresholds=True)

            if torch.rand([]) < STDP_freq:
                STDP_flag = True
                onehot_label = torch.nn.functional.one_hot(
                    labels,
                    num_classes=10
                ).float()
                reversed_label = self.label_off_value * (1.0 - onehot_label)
                self.cortex.STDP(onehot_label+reversed_label, **self.STDP_mechanism)

        if STDP_flag and self.self_cleaning_after_STDP:
            self.cortex.self_cleaning()

        return

    def __call__(self, images, adjust_thresholds=False):
        batch_size, channel, height, width = images.shape
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.cortex.init_state(batch_size, device)

        spike_train = []
        for spikes in self.image_encoder(images):
            output_spikes = self.cortex(spikes, adjust_thresholds)
            spike_train.append(output_spikes)
        return sum(spike_train)
