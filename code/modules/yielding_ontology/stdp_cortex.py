import torch
from modules.yielding_ontology.base_cortex import BaseCortex


class StdpCortex(BaseCortex):
    def __init__(
        self, STDP_cortex_settings, **kargs
    ):
        super().__init__(**kargs)

        def setup(init_P_rate, init_D_P_rate_ratio, learning_rate_settings, amplifier_settings):
            self.potentiation_rate = init_P_rate
            self.depression_rate = init_P_rate * init_D_P_rate_ratio
            self.learning_rate_settings = learning_rate_settings
            self.amplifier_settings = amplifier_settings
            self.amplifier_fixed = False

        setup(**STDP_cortex_settings)

    def fix_amplifier(self):
        self.amplifier_fixed = True

    def adjust_learning_rate(self, p_rate_adjust_base, optimal_output_thresholds, learning_rate_decrease_base):
        self.potentiation_rate *= 1+learning_rate_decrease_base
        self.depression_rate *= 1+learning_rate_decrease_base
        if p_rate_adjust_base == 0:
            return

        current_output_threshold = self.output_nv.mean_threshold_excluding_zeros
        if current_output_threshold < optimal_output_thresholds:
            self.potentiation_rate *= 1+p_rate_adjust_base
        else:
            self.potentiation_rate /= 1+p_rate_adjust_base

    def adjust_amplifier(self, amplifier_adjust_base, upper_bound, lower_bound, optimal_input_thresholds):
        if self.amplifier_fixed:
            return

        if amplifier_adjust_base == 0:
            return

        current_input_threshold = self.input_nv.mean_threshold_excluding_zeros
        if current_input_threshold < optimal_input_thresholds:
            self.amplifier *= 1 + amplifier_adjust_base
            self.amplifier = min(self.amplifier, upper_bound)
        else:
            self.amplifier /= 1 + amplifier_adjust_base
            self.amplifier = max(self.amplifier, lower_bound)

    def _mix_spikes_and_labels(self, labels, neuron_vector, label_usage):
        if label_usage == 'add':
            current_spikes = neuron_vector.current_spikes + labels
            spike_trace = neuron_vector.spike_trace
        elif label_usage == 'overwrite':
            current_spikes = labels
            spike_trace = neuron_vector.spike_trace
        elif label_usage == 'mask':
            current_spikes = neuron_vector.current_spikes * labels
            spike_trace = neuron_vector.spike_trace * labels
        elif label_usage == 'mask_only_potentiation':
            current_spikes = neuron_vector.current_spikes * labels
            spike_trace = neuron_vector.spike_trace
        return current_spikes, spike_trace

    def STDP(self, labels, ignore_label_after_brocasted, label_broadcasting, label_usage, learning_rate_factor_by_shape_ratio):
        input_len, output_len = self.shape

        self.adjust_learning_rate(**self.learning_rate_settings)
        self.adjust_amplifier(**self.amplifier_settings)

        B_labels_for_subcortexs_inputs = []
        for subcortex in self.subcortexs:
            subcortex_input_len, output_len = subcortex.shape

            B_labels_for_subcortexs_inputs.append(
                subcortex.STDP(
                    labels,
                    ignore_label_after_brocasted, label_broadcasting,
                    label_usage,
                    learning_rate_factor_by_shape_ratio
                )
            )

        if len(B_labels_for_subcortexs_inputs) > 0 and ignore_label_after_brocasted:
            labels *= 0

        #  ======= kernel modification start ========
        sender_spikes = self.input_nv.current_spikes
        sender_spike_trace = self.input_nv.spike_trace

        output_nv_current_spikes, output_nv_spike_trace = self._mix_spikes_and_labels(
            labels, self.output_nv, label_usage
        )
        receiver_current_spikes = [output_nv_current_spikes]
        receiver_spike_trace = [output_nv_spike_trace]

        # for subcortex in self.subcortexs:
        for B_label, subcortex in zip(B_labels_for_subcortexs_inputs, self.subcortexs):
            if label_broadcasting:
                sub_in_nv_current_spikes, sub_in_nv_spike_trace = self._mix_spikes_and_labels(
                    B_label, subcortex.input_nv, label_usage
                )
            else:
                sub_in_nv_current_spikes = subcortex.input_nv.current_spikes
                sub_in_nv_spike_trace = subcortex.input_nv.spike_trace
            receiver_current_spikes.append(sub_in_nv_current_spikes)
            receiver_spike_trace.append(sub_in_nv_spike_trace)

        receiver_current_spikes = torch.cat(receiver_current_spikes, axis=1)
        receiver_spike_trace = torch.cat(receiver_spike_trace, axis=1)

        potentiation_matrix = torch.einsum(
            'bi, bj -> ij',
            sender_spike_trace,
            receiver_current_spikes
        )
        depression_matrix = torch.einsum(
            'bi, bj -> ij',
            sender_spikes,
            receiver_spike_trace
        )

        if learning_rate_factor_by_shape_ratio:
            potentiation_matrix *= self.shape_ratio
            depression_matrix *= self.shape_ratio

        self.kernel += self.potentiation_rate * potentiation_matrix
        self.kernel -= self.depression_rate * depression_matrix
        self.kernel_regulation()
        #  ======= kernel modification end ========

        #  ======= calcuate broadcasted labels for input ========
        B_labels_for_all_recievers = torch.cat(
            [labels] + B_labels_for_subcortexs_inputs, axis=1
        )
        B_labels_for_input = torch.einsum(
            'br, ir -> bi',
            B_labels_for_all_recievers, self.kernel[:input_len, :]
        )

        return B_labels_for_input * self.amplifier
