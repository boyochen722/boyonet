import torch
import numpy as np
from sklearn.decomposition import PCA as sklearn_PCA
# to avoid centering trick,
# we implement our own version based on sklearn.decomposition.TruncatedSVD
from sklearn.decomposition import TruncatedSVD, NMF
from sklearn.model_selection import LeavePOut
from modules.yielding_ontology.stdp_cortex import StdpCortex


class PcaCortex(StdpCortex):
    def __init__(
        self, PCA_cortex_settings, **kargs
    ):
        super().__init__(**kargs)

        def setup(
            new_cortex_split_ratio, regularize_afterward,
            PCA_settings, weight_refund,
            refund_with_amplifier, split_only_tail,
            amplifier_base
        ):
            self.shape_fixed = False
            self.new_cortex_split_ratio = new_cortex_split_ratio
            self.regularize_afterward = regularize_afterward
            self.PCA_settings = PCA_settings
            self.weight_refund = weight_refund
            self.refund_with_amplifier = refund_with_amplifier
            self.split_only_tail = split_only_tail
            self.amplifier *= amplifier_base

        setup(**PCA_cortex_settings)

    def fix_shape(self):
        self.shape_fixed = True

    def self_cleaning(self):
        input_len, output_len = self.shape
        removed_receiver_indices = []
        simplified_kernel = self.kernel[:, :output_len].clone()
        i = len(self.output_nv)
        for subcortex in self.subcortexs:
            subcortex_inlen, subcortex_outlen = subcortex.shape
            sub_removed_indices, sub_simplified_kernel = subcortex.self_cleaning()
            removed_receiver_indices.append(sub_removed_indices+i)

            if self.weight_refund:
                if len(sub_removed_indices) > 0:
                    refund_weight = torch.einsum(
                        'Sr, rR -> SR',
                        self.kernel[:, sub_removed_indices+i],
                        sub_simplified_kernel[sub_removed_indices, :]
                    )
                    self.kernel[:, :output_len] += refund_weight
                    simplified_kernel += refund_weight

                # Keep only the indices that are not removed
                all_indices = torch.arange(subcortex_inlen, device=self.kernel.device)
                kept_indices_mask = ~torch.isin(all_indices, sub_removed_indices)
                sub_kept_indices = all_indices[kept_indices_mask]

                simplified_sub_weight = torch.einsum(
                    'Sk, kR -> SR',
                    self.kernel[:, sub_kept_indices+i],
                    sub_simplified_kernel[sub_kept_indices, :]
                )
                simplified_kernel += simplified_sub_weight

            i += subcortex_inlen

        # remove small threshold input (if shape is not fixed).
        if not self.shape_fixed:
            removed_indices = self.input_nv.delete_small_threshold_neurons()
            self._remove_kernel_indices(dim=0, indices=removed_indices)
        else:
            removed_indices = torch.tensor([])

        # remove small threshold subcortex input neurons
        if len(removed_receiver_indices) > 0:
            self._remove_kernel_indices(dim=1, indices=torch.cat(removed_receiver_indices))
        # if any subcortex has no input neuron:
        self._remove_dead_subcortexs()

        if self.refund_with_amplifier:
            simplified_kernel *= self.amplifier

        return removed_indices, simplified_kernel

    def _remove_dead_subcortexs(self):
        i = 0
        while i < len(self.subcortexs):
            sub_inlen, sub_outlen = self.subcortexs[i].shape
            if sub_inlen == 0:
                self.subcortexs.pop(i)
            else:
                i += 1

    def _remove_kernel_indices(self, dim, indices):
        mask = torch.ones(self.kernel.shape[dim], dtype=torch.bool, device=self.kernel.device)
        mask[indices] = False
        if dim == 0:
            self.kernel = self.kernel[mask, :]
        elif dim == 1:
            self.kernel = self.kernel[:, mask]
        else:
            raise ValueError(f"Invalid dim {dim}, expected 0 or 1")

    def normalizing_PCA(PCA_func):
        def wrap(self, kernel, normalizing, **kwargs):
            S_remaining, D_remaining, amplifier = PCA_func(self, kernel, **kwargs)

            if normalizing:
                epsilon = 1.0e-12
                S_L1_norm = abs(S_remaining).sum(axis=1).mean() + epsilon
                D_L1_norm = abs(D_remaining).sum(axis=1).mean() + epsilon
                S_remaining /= S_L1_norm
                D_remaining /= D_L1_norm
                amplifier *= S_L1_norm * D_L1_norm

            if normalizing == 'by_shape_ratio':
                S_shape_ratio = float(S_remaining.shape[1])/S_remaining.shape[0]
                D_shape_ratio = float(D_remaining.shape[1])/D_remaining.shape[0]
                S_remaining *= S_shape_ratio
                D_remaining *= D_shape_ratio
                amplifier /= S_shape_ratio * D_shape_ratio

            return S_remaining, D_remaining, amplifier
        return wrap

    def transposing_PCA(PCA_func):
        def wrap(self, kernel, transpose, **kwargs):
            if transpose:
                transposed_kernel = kernel.transpose()
            else:
                transposed_kernel = kernel

            S_remaining, D_remaining = PCA_func(self, transposed_kernel, **kwargs)

            if transpose:
                tmp = S_remaining.transpose()
                S_remaining = D_remaining.transpose()
                D_remaining = tmp

            return S_remaining, D_remaining
        return wrap

    def leave_P_out_PCA(PCA_func):
        def wrap(self, kernel, leave_out_num, **kwargs):
            if leave_out_num == 0:
                S_remaining, D_remaining = PCA_func(self, kernel, **kwargs)
                return S_remaining, D_remaining, 1

            lpo = LeavePOut(leave_out_num)
            S_remainings = []
            D_remainings = []
            split_count = 0
            for kept_index, left_out_index in lpo.split(np.arange(kernel.shape[-1])):
                split_count += 1
                masked_kernel = np.copy(kernel)
                masked_kernel[:, left_out_index] = 0.0
                S_remaining, D_remaining = PCA_func(self, masked_kernel, **kwargs)
                S_remainings.append(S_remaining)
                D_remainings.append(D_remaining)

            amplifier = 1.0 / max(split_count, 1)
            return np.concatenate(S_remainings, axis=1), np.concatenate(D_remainings, axis=0), amplifier
        return wrap

    @normalizing_PCA
    @leave_P_out_PCA
    @transposing_PCA
    def PCA(self, kernel, remained_variance, method):
        # method in {'PCA', 'non_centering_PCA', 'PNMF'}
        if method == 'PCA':
            if remained_variance == 1:
                # parameter for sklearn.decomposition.PCA
                # https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html
                remained_variance = None
            pca = sklearn_PCA(svd_solver='full', n_components=remained_variance)
            S_remaining = pca.fit_transform(kernel)
            D_remaining = pca.components_
        elif method == 'non_centering_PCA':
            sender_num, receiver_num = kernel.shape
            svd = TruncatedSVD(n_oversamples=receiver_num, n_components=receiver_num)
            S_full = svd.fit_transform(kernel)
            D_full = svd.components_

            # calculate how many components to keep
            remaining_components_num = np.searchsorted(
                np.cumsum(svd.explained_variance_ratio_), remained_variance
            )+1

            S_remaining = S_full[:, :remaining_components_num]
            D_remaining = D_full[:remaining_components_num, :]
        elif method == 'PNMF':
            if remained_variance == 1:
                # parameter for sklearn.decomposition.PCA
                # https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html
                remained_variance = None

            kernel_pos = np.maximum(kernel, 0)
            kernel_neg = np.maximum(-kernel, 0)

            model_pos = NMF(n_components=remained_variance, init='random', random_state=722)
            W_pos = model_pos.fit_transform(kernel_pos)
            H_pos = model_pos.components_

            model_neg = NMF(n_components=remained_variance, init='random', random_state=722)
            W_neg = model_neg.fit_transform(kernel_neg)
            H_neg = -model_neg.components_

            S_remaining = np.concatenate([W_pos, W_neg], axis=1)
            D_remaining = np.concatenate([H_pos, H_neg], axis=0)

        return S_remaining, D_remaining

    def splitting(self, cortex_constructor):
        # recursive into subcortex first to prevent infinite splitting!
        for subcortex in self.subcortexs:
            subcortex.splitting(cortex_constructor)

        if self.split_only_tail and len(self.subcortexs) > 0:
            return

        h, w = self.shape
        if h < 2 or w < 2:
            return

        new_cortex_in_links, right_matrix, amplifier = self.PCA(
            np.array(self.kernel[:h, :w]),
            **self.PCA_settings
        )

        new_cortex_in_links *= self.new_cortex_split_ratio

        summarized_new_kernel = np.einsum('in,no->io', new_cortex_in_links, right_matrix) * amplifier
        left_old_IO_kernel = np.array(self.kernel)[:, :w] - summarized_new_kernel
        old_sub_kernel = np.array(self.kernel)[:, w:]

        new_cortex_kernel = right_matrix
        new_cortex_input_len, output_len = new_cortex_kernel.shape

        new_kernel = np.concatenate(
            [left_old_IO_kernel, old_sub_kernel, new_cortex_in_links], axis=1
        )

        self.kernel = torch.tensor(new_kernel)
        if self.regularize_afterward:
            self.kernel_regulation()

        # create new cortex
        self.subcortex_counter += 1
        self.subcortexs.append(
            cortex_constructor(
                cortex_id=f'{self.cortex_id}-{self.subcortex_counter}',
                input_len=new_cortex_input_len,
                output_len=self.shape[1],
                kernel=torch.tensor(new_cortex_kernel),
                amplifier=amplifier
            )
        )
