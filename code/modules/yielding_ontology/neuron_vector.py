import torch
from modules.utils import mean_excluding_zeros


class NeuronVector():
    def __init__(
        self, length, spike_fade_rate, reset_state, potential_fade_rate, noise_settings,
        threshold_weighting_power, minimal_threshold, threshold_setting, optimal_fire_rate,
        balanced_recent_spike, resting_length, input_activaton, unified_threshold=False
    ):
        self.spike_fade_rate = spike_fade_rate
        self.reset_state = reset_state
        self.resting_length = resting_length
        self.potential_fade_rate = potential_fade_rate
        self.noise_settings = noise_settings

        self.threshold_type = threshold_setting.get('type', 'power')
        self.threshold_base = 1.0 + threshold_setting.get('base', 0)
        self.threshold_values = torch.ones([length])
        self.unified_threshold = unified_threshold
        self.input_activaton = input_activaton
        self.threshold_weighting_power = threshold_weighting_power
        self.minimal_threshold = minimal_threshold

        if balanced_recent_spike:
            self.recent_spike_factor = (1-spike_fade_rate) / spike_fade_rate
        else:
            self.recent_spike_factor = 1
        self.threshold_up_per_fired = (1-optimal_fire_rate) / optimal_fire_rate

    def __len__(self):
        return int(self.threshold_values.shape[0])

    @property
    def mean_threshold_excluding_zeros(self):
        return mean_excluding_zeros(self.thresholds)

    @property
    def thresholds(self):
        if self.threshold_type == 'sigmoid':
            thresholds = torch.sigmoid(self.threshold_values) * self.threshold_base
        elif self.threshold_type == 'power':
            thresholds = torch.pow(self.threshold_base, self.threshold_values)
        elif self.threshold_type == 'linear':
            thresholds = self.threshold_values * self.threshold_base
        elif self.threshold_type == 'tanh':
            thresholds = torch.tanh(self.threshold_values) + self.threshold_base
        else:
            raise ValueError(f"Unsupported threshold type: {self.threshold_type}")

        if self.unified_threshold:
            thresholds = torch.ones([len(self)]) * torch.mean(thresholds)
        return thresholds

    def init_state(self, batch_size, device):
        self.potentials = torch.zeros([batch_size, len(self)], device=device)
        self.resting_period = torch.zeros([batch_size, len(self)], device=device)
        self.current_spikes = torch.zeros([batch_size, len(self)], device=device)
        self.spike_trace = torch.zeros([batch_size, len(self)], device=device)

    def add_additional_spikes(self, additional_spikes, maintain_magnitude, magnitude_axis, ord=1):
        if maintain_magnitude in ['soft', 'strict']:
            epsilon = 1e-12
            original_norm = torch.norm(self.current_spikes, p=ord, dim=magnitude_axis) + epsilon
            additional_spikes_norm = torch.norm(additional_spikes, p=ord, dim=magnitude_axis) + epsilon

            current_spikes_factor = torch.nn.functional.relu(
                original_norm - additional_spikes_norm
            ) / original_norm
            if magnitude_axis == 1:
                self.current_spikes = torch.einsum(
                    'b, bi->bi',
                    current_spikes_factor, self.current_spikes
                )
            elif magnitude_axis == 0:
                self.current_spikes = torch.einsum(
                    'i, bi->bi',
                    current_spikes_factor, self.current_spikes
                )

            if maintain_magnitude == 'strict':
                additional_spikes_factor = torch.min(
                    original_norm/additional_spikes_norm, torch.tensor(1.0)
                )
                if magnitude_axis == 1:
                    additional_spikes = torch.einsum(
                        'b, bi->bi',
                        additional_spikes_factor, additional_spikes
                    )
                elif magnitude_axis == 0:
                    additional_spikes = torch.einsum(
                        'i, bi->bi',
                        additional_spikes_factor, additional_spikes
                    )

        self.current_spikes += additional_spikes

    def add_noise(self, spike_vector, noise_type, noise_rate):
        # Generate a noise vector of the same shape as spike_vector with random values
        noise_vector = torch.rand(spike_vector.shape, device=spike_vector.device) < noise_rate
        bool_spike_vector = spike_vector.bool()

        if noise_type == 'or':
            bool_noisy_spikes = torch.logical_or(bool_spike_vector, noise_vector)
        elif noise_type == 'xor':
            bool_noisy_spikes = torch.logical_xor(bool_spike_vector, noise_vector)
        else:
            raise ValueError(f"Unsupported noise type: {noise_type}")
        
        noisy_spikes = bool_noisy_spikes.float()
        return noisy_spikes

    def _record_spike_trace(self, recent_spike):
        self.spike_trace *= self.spike_fade_rate
        self.spike_trace += recent_spike * self.recent_spike_factor

    def stimulate(self, input_spikes, adjust_thresholds=False):
        if self.input_activaton == 'tanh':
            input_spikes = torch.tanh(input_spikes)
        elif self.input_activaton == 'clip':
            input_spikes = torch.clamp(input_spikes, max=1, min=-1)

        rest_mask = (self.resting_period > 0).float()

        self.potentials *= self.potential_fade_rate
        self.potentials += input_spikes * (1.0-rest_mask)
        newly_fired = (self.potentials > self.thresholds).float()
        newly_fired = self.add_noise(newly_fired, **self.noise_settings)

        # reset state
        self.potentials += newly_fired * (self.reset_state - self.potentials)

        # resting period
        self.resting_period -= rest_mask
        self.resting_period += newly_fired * self.resting_length

        if adjust_thresholds:
            self.threshold_values += torch.sum(newly_fired, dim=0) * (1+self.threshold_up_per_fired)
            self.threshold_values -= (newly_fired.shape[0])

        self._record_spike_trace(self.current_spikes)
        self.current_spikes = newly_fired * (self.thresholds ** self.threshold_weighting_power)

    def delete_small_threshold_neurons(self):
        # Create a boolean mask for thresholds greater than or equal to the minimal threshold
        valid_thresholds_mask = self.thresholds >= self.minimal_threshold

        # Find the indices where thresholds are smaller than the minimal threshold
        removed_indices = torch.where(~valid_thresholds_mask)[0]

        # Apply the mask to threshold_values to filter out small thresholds
        self.threshold_values = self.threshold_values[valid_thresholds_mask]

        return removed_indices
