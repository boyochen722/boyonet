import torch
from collections import deque
from modules.utils import remove_nan
from modules.yielding_ontology.neuron_vector import NeuronVector


class BaseCortex():
    def __init__(
        self, cortex_id,
        input_len, output_len,
        neuron_vectors_spec,
        base_cortex_settings,
        subcortexs=None,
        kernel=None,
        amplifier=1
    ):

        def setup(
            normalize_settings, no_skip_links, init_std, init_mean,
            minimal_weight, input_latency, lateral_inhibition_factor
        ):
            self.normalize_settings = normalize_settings
            self.no_skip_links = no_skip_links
            self.minimal_weight = minimal_weight
            self.input_latency = input_latency
            self.lateral_inhibition_factor = lateral_inhibition_factor
            self.init_std = init_std
            self.init_mean = init_mean

        setup(**base_cortex_settings)

        self.amplifier = amplifier
        self.cortex_id = cortex_id
        self.nv_spec = neuron_vectors_spec

        self.input_nv = NeuronVector(input_len, **self.nv_spec)
        self.output_nv = NeuronVector(output_len, **self.nv_spec)

        if subcortexs is None:
            self.subcortexs = []
            self.subcortex_counter = 0
        else:
            self.subcortexs = subcortexs
            self.subcortex_counter = len(subcortexs)

        if kernel is None:
            kernel_shape = [input_len, output_len]
            for subcortex in self.subcortexs:
                sub_in_len, sub_out_len = subcortex.shape
                kernel_shape[1] += sub_in_len
            self.kernel = torch.randn(kernel_shape) * self.init_std + self.init_mean
        else:
            self.kernel = kernel
        self.kernel_regulation()

    @property
    def shape(self):
        return (len(self.input_nv), len(self.output_nv))

    @property
    def shape_ratio(self):
        # shape of kernel, not input/output.
        sender_len, receiver_len = self.kernel.shape
        return float(receiver_len) / sender_len

    def remove_skip_links(self):
        h_start, w_start = self.shape
        # remove all links from input to output
        self.kernel[:h_start, :w_start] = 1e-12

    def prune_kernel(self, minimal_weight):
        self.kernel = torch.where(
            condition=torch.abs(self.kernel) >= minimal_weight,
            input=self.kernel,
            other=0.0
        )

    def normalize_kernel(self, normalize_method, epsilon, out_weight_len, gently=False):
        if normalize_method == 'None':
            return

        self.kernel += epsilon
        if normalize_method == 'softmax':
            kernel_sign = torch.sign(self.kernel)
            normalized_kernel = kernel_sign * torch.softmax(
                torch.abs(self.kernel), dim=1
            )
        elif normalize_method == 'L1':
            normalized_kernel = torch.nn.functional.normalize(
                self.kernel, p=1, dim=1
            )
        elif normalize_method == 'L2':
            normalized_kernel = torch.nn.functional.normalize(
                self.kernel, p=2, dim=1
            )
        if out_weight_len == 'by_shape_ratio':
            out_weight_len = self.shape_ratio
        if gently:
            self.kernel += normalized_kernel * out_weight_len
            self.kernel /= 2
        else:
            self.kernel = normalized_kernel * out_weight_len

    def kernel_regulation(self):
        if len(self.subcortexs) > 0 and self.no_skip_links:
            self.remove_skip_links()
        self.normalize_kernel(**self.normalize_settings)
        self.prune_kernel(self.minimal_weight)

        self.kernel = remove_nan(self.kernel, 0)

    def init_state(self, batch_size, device):
        self.input_queue = deque()
        for i in range(self.input_latency):
            self.input_queue.append(
                torch.zeros([batch_size, len(self.input_nv)], device=device)
            )

        self.input_nv.init_state(batch_size, device)
        self.output_nv.init_state(batch_size, device)
        for subcortex in self.subcortexs:
            subcortex.init_state(batch_size, device)

    def __call__(self, input_spikes, adjust_thresholds=False):
        self.input_queue.append(input_spikes)
        self.input_nv.stimulate(
            self.input_queue.popleft() * self.amplifier,
            adjust_thresholds=adjust_thresholds
        )

        sender_spikes = self.input_nv.current_spikes

        # update potentials with spikes
        update_potentials = torch.einsum(
            'bi, io -> bo',
            sender_spikes, self.kernel
        )
        lateral_inhibition = torch.sum(
            self.output_nv.current_spikes, dim=1, keepdim=True
        ) * self.lateral_inhibition_factor

        torch.sum(self.output_nv.current_spikes, dim=1)

        i = len(self.output_nv)
        potential_to_output = update_potentials[:, :i]

        for subcortex in self.subcortexs:
            vector_length = subcortex.shape[0]
            potential_to_output += subcortex(
                update_potentials[:, i:i+vector_length],
                adjust_thresholds=adjust_thresholds
            )
            i += vector_length

        potential_to_output -= lateral_inhibition

        self.output_nv.stimulate(
            potential_to_output,
            adjust_thresholds=adjust_thresholds
        )
        return self.output_nv.current_spikes
