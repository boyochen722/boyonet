FROM nvcr.io/nvidia/pytorch:24.02-py3

EXPOSE 6090 6091

# Install miniconda
ENV CONDA_DIR /opt/conda
RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-py310_24.7.1-0-Linux-x86_64.sh -O ~/miniconda.sh && \
     /bin/bash ~/miniconda.sh -b -p /opt/conda

# Put conda in path so we can use conda activate
ENV PATH=$CONDA_DIR/bin:$PATH

# default to use the first to forth GPU
ENV CUDA_VISIBLE_DEVICES 0,1,2,3

# # install node.js npm
# RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash - \
# 	&& apt-get install -y nodejs

#========================
WORKDIR /app
COPY ./environment.yaml ./run_server.sh /app/
COPY ./jupyter_config.py /root/.jupyter/jupyter_notebook_config.py

# install environment from environment.yaml
RUN conda env create -n boyonet -f environment.yaml -y
RUN conda init
RUN echo 'conda activate boyonet' >> ~/.bashrc

# install jupyterlab_tensorboard
RUN conda run -n boyonet pip install jupyterlab_tensorboard_pro

ENTRYPOINT ["conda", "run", "--no-capture-output", "-n", "boyonet", "./run_server.sh"]
