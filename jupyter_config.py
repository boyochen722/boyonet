## The IP address the notebook server will listen on.
#  Default: 'localhost'
c.NotebookApp.ip = '*'

## Whether to open in a browser after starting. The specific browser used is
#  platform dependent and determined by the python standard library `webbrowser`
#  module, unless it is overridden using the --browser (NotebookApp.browser)
#  configuration option.
#  Default: True
c.NotebookApp.open_browser = False

## Hashed password to use for web authentication.
#  The string should be of the form type:salt:hashed-password.
#  Default: ''
c.NotebookApp.password = 'argon2:$argon2id$v=19$m=10240,t=10,p=8$/a1w/7FlJyJTVpeH1fKQyw$/xTMdIgdHvlMjW3RBzp63Q'
