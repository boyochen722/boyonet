export docker_user_name='boyochen'
export folders=('code' 'logs')
export ports=('6090-6091')
export docker_image_name="${docker_user_name##*/}/${PWD##*/}"
# export proxy_server='1.1.1.1'
# export data_folder='../data'


# ------------------------------------
# --gpus all: access to every gpus
# -d detach after run

echo ======== build ========
echo docker build -t "${docker_image_name}" .

echo ========= run =========
echo docker run --gpus all -d \\

if [ -n "${data_folder}" ]; then
  absolute_data_folder="$(cd "$(dirname $data_folder)"; pwd)/$(basename $data_folder)"
  echo ' ' -v $absolute_data_folder:/app/data \\
fi

for folder in ${folders[*]}; do
	echo ' ' -v $(pwd)/$folder:/app/$folder \\
done

for port in ${ports[*]}; do
	echo ' ' -p $port:$port \\
done
echo ' ' --shm-size 8G \\


echo ' ' "${docker_image_name}"

# echo === reversed tunnel ===
# for port in ${ports[*]}; do
# 	echo ssh -fNR $proxy_server:$port:localhost:$port $proxy_server
# done
